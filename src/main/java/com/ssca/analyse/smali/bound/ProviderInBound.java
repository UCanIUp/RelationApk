package com.ssca.analyse.smali.bound;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.ssca.analyse.smali.bound.parser.UriMatcherAddURI;
import com.ssca.analyse.smali.controlflow.CodeMapItemMapper;
import com.ssca.analyse.smali.controlflow.FileData;
import com.ssca.analyse.smali.controlflow.FunctionData;
import com.ssca.commonData.CommonData;
import com.ssca.commonData.IntentData;

/**
 * @author lichengze, 2017.05.22
 */

public class ProviderInBound {

	// private static Logger logger =
	// LogManager.getLogger(ActivityInBound.class);

	private Map<String, FileData> SmaliMap;

	public void getInBound() {
		// get smali file control flow map
		SmaliMap = CodeMapItemMapper.getInstance().getSmaliFileMap();

		for (Entry<String, FileData> SmaliMapEntry : SmaliMap.entrySet()) {
			// get smali filedata
			FileData SmaliFileData = SmaliMapEntry.getValue();
			// logger.info("start analyse smali dataflow: " +
			// SmaliMapEntry.getKey() + "");
			analyseSmaliFileData(SmaliFileData);
			// logger.info("end analyse smali dataflow: " +
			// SmaliMapEntry.getKey() + "");
		}
	}

	private void analyseSmaliFileData(FileData SmaliFileData) {
		List<FunctionData> functionList = SmaliFileData.getFunList();
		for (FunctionData functionData : functionList) {
			// TODO 1. UriMatcher.addURI();
			UriMatcherAddURI parse1 = new UriMatcherAddURI(functionData);
			List<IntentData> reList1 = parse1.uriParse("addURI");

			// save result to commonData
			if (reList1.size() >= 1) {
				if (CommonData.getProviderInList() == null || CommonData.getProviderInList().size() == 0)
					CommonData.setProviderInList(reList1);
				else {
					List<IntentData> alreadyIn = CommonData.getProviderInList();
					alreadyIn.addAll(reList1);
					CommonData.setProviderInList(alreadyIn);
				}
			}

			// // TODO 2. Uri.parse();
			// UriParse parse2 = new UriParse(functionData);
			// List<IntentData> reList2 = parse2.uriParse("parse");
			//
			// // save result to commonData
			// if (reList2.size() >= 1) {
			// if (CommonData.getProviderInList() == null ||
			// CommonData.getProviderInList().size() == 0)
			// CommonData.setProviderInList(reList2);
			// else {
			// List<IntentData> alreadyIn = CommonData.getProviderInList();
			// alreadyIn.addAll(reList2);
			// CommonData.setProviderInList(alreadyIn);
			// }
			// }

		}
	}

}
